import React from 'react';
import { View } from 'react-native';
import Header from './component/Header';
import AlbumList from './component/AlbumList';

export default class App extends React.Component {
  render() {
    return (
      <View style={{ flex: 1 }}>
        <Header HeaderText={'Albums'} />
        <AlbumList />
      </View>  
    );
  }
}
